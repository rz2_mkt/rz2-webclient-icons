# Webclient Icons - RZ2

As fonts são geradas utilizando o site [iconmoon](https://icomoon.io/).

## Atualizando ícones

* importar o _.json_ da pasta **source**, em **Import Project**
* importar o(s) novo(s) ícone(s)
* gerar a fonte, produzindo um arquivo **iconmoon.zip**
* extrair a pasta _fonts_ e o _styles.css_ para a pasta **dist**
* colocar os **.svg** dos ícones, nas pasta **source**
* exportar o projeto (arquivo _.json_) e colocá-lo na pasta **source**
  * Em [projects](https://icomoon.io/app/#/projects), opção **download**

## Utilizando os ícones
Instalar utlizando o **npm**

```
npm install --save https://bitbucket.org/christiangf/rz2-webclient-icons
```
Referenciar no _.angular-cli.json_, na seção _styles_, o arquivo **.css**:

```
"styles": [
  ...

  "../node_modules/webclient-icons/dist/style.css",

  ...
],
```

